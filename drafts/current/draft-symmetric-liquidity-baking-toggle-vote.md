---
title: Liquidity Baking Toggle Vote
status: Draft
author: Raphaël Cauderlier <raphael.cauderlier@nomadic-labs.com>
type: Protocol
created: 2022-01-18
date: 2022-02-27
version: 0
---

## Summary

This TZIP proposes several modifications to the rules of the Escape
Vote of Liquidity Baking. The main modifications are:

- a renaming of the options, they are now called "**On**" and
  "**Off**";
- the addition of a third option named "**Pass**" allowing bakers to
  produce blocks that don't alter the state of the vote;
- a reset of the threshold to 50% (same value as in Granada and
  Hangzhou);
- a change in the semantics of the **Off** option: instead of
  permanently deactivating the subsidy it will now only deactivate it
  as long as **Off** votes have a majority (are more numerous than
  **On** votes), and;
- a renaming of the mechanism itself, from "Liquidity Baking Escape
  Vote" to "Liquidity Baking Toggle Vote".

## Motivation

The aim of this TZIP is to ensure that the subsidy is sent to the
Liquidity Baking contract if and only if a majority of the bakers
participating in the vote support the feature.

A drawback of the Escape Vote mechanism of Liquidity Baking as
currently implemented in Hangzhou is the lack of symmetry. The default
behaviour for bakers is to vote for the continuation of the Liquidity
Baking subsidy hence it does not allow to distinguish the bakers
supporting the Liquidity Baking feature from the ones relying on the
default configuration of their baking daemon.

To achieve its goal, this TZIP first proposes to let the bakers choose
to participate in the vote or not by adding a third option called
"**Pass**" and second it makes the vote symmetric so that the will of
the majority can clearly be identified. Finally, to reduce confusion,
the options previously named "**true**" and "**false**" are renamed
"**Off**" and "**On**" respectively and the mechanism is renamed
"Liquidity Baking Toggle Vote".

## Specification

The Toggle Vote of Liquidity Baking relies on the computation of an
exponential moving average (EMA) of the signals sent by bakers in
their blocks. These signals can have three possible values: **On**,
**Off**, and **Pass**. The EMA is updated once per block as follows:
- if the baker of the block chose the **Pass** option then the EMA is
  not modified,
- if the baker of the block chose the **Off** option then the EMA is
  increased,
- if the baker of the block chose the **On** option then the EMA is
  decreased.

At each block, the EMA is compared to a constant known as the the
_threshold_ of the Toggle Vote. When the EMA is above the threshold,
it means that **Off** votes have the majority. On the opposite, a
value below the threshold means that **On** votes have the majority.

The subsidy is sent to the Liquidity Baking contract if and only if
the block level is below the liquidity baking sunset level and the EMA
is below the threshold.

More precisely, the EMA is a natual number whose value can vary
between 0 and 2 billion and the threshold is 1 billion.
In each block, the EMA is updated as follows:
- if the baker votes **Pass** then the value is unchanged: EMA[n+1] =
  EMA[n]
- if the baker votes **On** then EMA[n+1] = (EMA[n] * 1999) / 2000
- if the baker votes **Off** then EMA[n+1] = ((EMA[n] * 1999) /
  2000) + 1,000,000

### Technical details

To ensure symmetry between the **On** and the **Off** options, the
division is always rounded toward the threshold. The EMA is
represented as a 32-bit signed integer. To avoid overflows, the
multiplication is not performed on 32-bit integers but using
arbitrary-precision arithmetics; 32-bit numbers are used to store the
EMA across blocks and to compare it with the threshold. Performing the
multiplication on arbitrary-precision numbers also let us increase the
precision of the computation of the EMA; compared to Granada, Hangzhou
and the Ithaca proposals, the EMA has been scaled by a factor of 1000.

### Migration

During migration from a protocol not containing the changes proposed
in this document the following must be done:
- If at the moment of the migration the ema is above the threshold,
  then it means that the Liquidity Baking Subsidy has been permanently
  deactivated according to the rules of the previous protocol. To
  respect the choice of the bakers, the feature must be permanently
  deactivated in the new protocol too; this can be done for example by
  setting the sunset level in the past.
- The protocol constant for the threshold must be set to 1 billion.
- The EMA must be multiplied by 1000 to account for the rescaling.

## Rationale

Two other changes to the Liquidity Baking Escape Vote have been
mentioned:
- The Ithaca and Ithaca 2 protocol proposals introduce a reduction of
  the escape threshold. It allows a minority (the concrete value in
  Ithaca is 1/3 of the stake) to permanently deactivate the
  subsidy. This reduction is meant to account for the fact that not
  all non-signaling bakers are supporters of the Liquidity Baking
  feature.
- The opposite change would consist in raising the threshold (for
  example to 2/3 of the stake) and make voting for escaping the
  subsidy the default option.

In both cases, the system is asymmetric and tries to account for the
asymmetry by lowering the requirement of the camp that does not
benefit from default votes. The main advantage of these options is
that they don't add any complexity to the protocol. Their main
drawback is that they are only fair to both camps if the proportion of
bakers relying on the default behavior is known *a priori*.

The addition of the **Pass** option also raises
[questions](https://gitlab.com/tezos/tezos/-/merge_requests/4201#note_811232512)
on the legitimacy of the vote, especially if only a small minority of
bakers engage in the vote. A participation quorum could be added to
the system but it raises new design questions: Which participation
rate would be reasonable to require? How to make the quorum system
symmetric? How to avoid discontinuities when the participation is
close to the required rate?  We don't think that adding a
participation quorum is worth the added complexity, especially if the
halt of the subsidy is not permanent anymore. We also encourage
developers of baking software to not propose a default behaviour for
how the baker votes and instead make picking one of the three options
mandatory.

## Backwards Compatibility

The changes to rename the vote options and the addition of **Pass**
require non-backward compatible changes to the binary and JSON
encodings of these options. This change affects the encodings of block
headers. The following table summarizes the change:

| Vote option | Old binary encoding | Old JSON encoding | New binary encoding | New JSON encoding |
| ----------- | ------------------- | ----------------- | ------------------- | ----------------- |
| **On**      | `0x00`              | `false`           | `0x00`              | "on"              |
| **Off**     | any other byte      | `true`            | `0x01`              | "off"             |
| **Pass**    | N/A                 | N/A               | `0x02`              | "pass"            |

## Security Considerations

The security of the chain is not expected to be affected by this change.

## Test Cases

The escape hatch mechanism is already well tested. In addition to the
existing tests, we add the following:

### Unit tests for the functions computing the EMA

The following properties are tested on the functions computing the
EMA:

- Voting **Pass** does not affect the EMA.
- The EMA always stays between 0 and 2 billion.
- Voting **On** strictly decreases the EMA if it is above one thousand.
- Voting **On** does not decrease the EMA by more than 1 million.
- Voting **Off** strictly increases the EMA if it is below 1.999 billion.
- Voting **Off** does not increase the EMA by more than 1 million.
- Voting **On** is symmetric to voting **Off**: from two dual values of the EMA (that is, two values x and y such that x + y = 2,000,000,000), voting **On** on the first one decreases it by as much than voting **Off** on the second one increases it.
- 1386 **Off** blocks are needed to move the EMA from 0 to the threshold.

### Integration tests for non-permanent deactivation of the Liquidity Baking subsidy

The following new scenario has been added:

1. The EMA is initially at 0 (so the subsidy is active).
2. 2,000 blocks are produced with the vote flag set to **Off**.
3. We expect to be in a state where the subsidy is now inactive, we
   test this by producing a few blocks with the vote flag set to
   **Pass** and observe that the balance of the Liquidity Baking
   contract does not change.
4. 2,000 blocks are produced with the vote flag set to **On**.
5. We expect to be in a state where the subsidy is now active again, we
   test this by producing a few blocks with the vote flag set to
   **Pass** and observe that the balance of the Liquidity Baking
   contract increases by exactly the amount of the subsidy multiplied
   by the number of baked blocks.

## Implementations

https://gitlab.com/tezos/tezos/-/merge_requests/4201

## Appendix

- https://forum.tezosagora.org/t/proposal-plans/4160
- https://forum.tezosagora.org/t/symmetric-liquidity-baking-escape-vote/4250

## Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
